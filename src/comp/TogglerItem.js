import React from 'react'

const TogglerItem = (props) => {
    return(
        <div className={props.isActive ? 'btn active' : 'btn'} onClick={props.toggle} data-name={props.name}>
            {props.children}
        </div>
    )
};
export default TogglerItem;