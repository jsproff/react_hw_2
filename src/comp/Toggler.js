import React, { Component } from 'react'

export default class Toggler extends Component {
    constructor(props) {
        super(props);
        this.state = {
            togData: {
                name: ''
            }
        }
    }
    handlerChange = (event) => {
        const target = event.target;
        const val = target.dataset.name;
        this.setState({
            togData: {
                name: val
            }
        });
    };
    componentDidMount(){
        const defaultState = this.props.defaultState;
        this.setState({
            togData: {
               name: defaultState
            }
        });
    }
    render() {
        const { children } = this.props;
        const curActive = this.state.togData.name;
        return (
            <div className="wrapper">
                {
                    React.Children.map(
                        children,
                        (ChildrenItem) => {
                            if (ChildrenItem.props.name === curActive) {
                                // https://reactjs.org/docs/react-api.html#cloneelement
                                return React.cloneElement(ChildrenItem, {
                                    name: ChildrenItem.props.name,
                                    isActive: true,
                                    toggle: this.handlerChange
                                })
                            } else {
                                return React.cloneElement(ChildrenItem, {
                                    name: ChildrenItem.props.name,
                                    toggle: this.handlerChange
                                })
                            }
                        }
                    )
                }
            </div>
        )
    }
};

// export default Toggler;