import React, { Component } from 'react';
import './App.css';
import Toggler from './comp/Toggler'
import TogglerItem from './comp/TogglerItem'

class App extends Component {
  constructor(props) {
      super(props);
  }

  render() {
    return (
      <div className="App">
        <Toggler defaultState='tog1'>
            <TogglerItem name="tog1">left</TogglerItem>
            <TogglerItem name="tog2">centre</TogglerItem>
            <TogglerItem name="tog3">right</TogglerItem>
            <TogglerItem name="tog4">baseline</TogglerItem>
        </Toggler>
        <br/>
        <Toggler defaultState='male'>
            <TogglerItem name="male">male</TogglerItem>
            <TogglerItem name="female">female</TogglerItem>
        </Toggler>
      </div>
    );
  }
}

export default App;
